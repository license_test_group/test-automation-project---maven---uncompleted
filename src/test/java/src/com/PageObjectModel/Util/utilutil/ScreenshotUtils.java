package src.com.PageObjectModel.Util.utilutil;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import Sources.TestBase;

public class ScreenshotUtils extends TestBase {

	public ScreenshotUtils() throws IOException {
		super();
		// TODO Auto-generated constructor stub
	}
	public static void screenshot(String name) throws IOException {
		File srcFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFile, new File("E:\\Study\\Automation Testing\\PageObjectModel\\ScreenShots\\"+name+"title.png"));
	}

}
